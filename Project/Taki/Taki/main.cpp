#include "connection.h"
#include "Manager.h"
#include <thread>
#include <vector>
#include "Communicate.h"

#pragma comment(lib, "Ws2_32.lib")

Manager m;
std::vector <std::thread*> clientPool;
int poolCounter = 0;

int main() 
{
	bool running = true;
	addrinfo * res = NULL;
	SOCKET * client = NULL;
	SOCKET * serv = NULL;
	std::string shut;
	connection c(27015);
	res = c.Init();
	c.boot(res);
	

	while (true)
	{
		serv = c.getServer();
		client = c.getClient();
		*client = accept(*serv, NULL, NULL);
		if (*client != SOCKET_ERROR)
		{
			clientPool.push_back(new std::thread(Communicate::com, c, &m, poolCounter));
			poolCounter++;
		}
	}


	return 0;
}