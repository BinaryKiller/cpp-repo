#include "User.h"


User::User(std::string name, SOCKET sock)
{
	user_name = name;
	user_socket = sock;
}

SOCKET User::getSocket()
{
	return this->user_socket;
}

std::string User::getName()
{
	return user_name;
}