#include <winsock2.h>
#include <windows.h>
#include <ws2tcpip.h>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <stdio.h>


#ifndef _my_connection
#define _my_connection

class connection
{
public:
	connection(int port);
	struct addrinfo * Init();
	int boot(struct addrinfo * result);
	int shut();
	SOCKET* getClient();
	SOCKET* getServer();
	

private:
	WSAData _wsa;
	struct addrinfo _hints;
	SOCKET _listenSock;
	SOCKET _clientSock;
	int _port;
	
};
#endif