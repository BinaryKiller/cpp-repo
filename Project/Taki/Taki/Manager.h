#include <map>
#include "Room.h"
#include "connection.h"
#include <thread>
#include "sqlite3\sqlite3.h"

#ifndef _my_Manager
#define _my_Manager

class Manager
{
public:
	bool register_user(User& user, std::string password);
	void login_user(User &user);
	bool is_exist(User &user) const;
	Room * inRoom(User& user);
	User* try_login(std::string user_name, std::string user_password);
	bool add_room(User &admin, std::string room_name);
	bool remove_room(Room &room);
	bool start_game_db(const Room &room) const;
	bool end_game_db(const Room &room) const;
	void client_requests_thread(SOCKET sock);
	User& getUser(SOCKET sock);
	bool RoomExists(User& Admin);
	Room * getRoom(User& Admin);
	Room* getRoomByAdminName(std::string name);
	std::string list_Rooms();
	static int callback(void *NotUsed, int argc, char **argv, char **azColName);
	Manager();


private:
	short int gameCount;
	std::map<SOCKET, User *> user_map;
	std::vector<Room> room_vector;
	sqlite3 * db;

};

#endif