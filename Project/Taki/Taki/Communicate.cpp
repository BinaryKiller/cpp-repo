#include "Communicate.h"






void Communicate::com(connection c,Manager * m ,int id)
{
	bool logged = false;
	bool in_Room = false;
	char buff[100] = "";
	std::string res = "";
	SOCKET client = *c.getClient();
	std::vector<std::string> com;
	Room * currRoom = NULL;
	int iRes = recv(client, buff, 100, 0);
	std::string messageCode(buff);
	com = Explode(messageCode);
	messageCode = com[0];
	while (messageCode != "EN_LOGOUT")
	{
		if (!logged)
		{
			if (messageCode == "EN_REGISTER")
			{
				if (com.size() == 3)
				{
					if ((com[1].length() > 20) || (com[2].length() > 20))
					{
						//the params(user name and password) arent legal(or at least one of them)
						send(client, "@PGM_ERR_REGISTERR_INFO||", strlen("@PGM_ERR_REGISTERR_INFO||"), 0);
					}
					else
					{
						/*username should be inside com[1]
						password should be inside com[2]
						write them down into the sqlite "server",implement it at manager::register_user*/

						m->register_user(*(new User(com[1], client)), com[2]);

						//when done registering the user,send him the successfull register command:
						res.append("@PGM_SCC_REGISTER|");
						res.append(m->list_Rooms());			//return the list of rooms too
						send(client, res.c_str(), strlen(res.c_str()), 0); //pack it all up,and send it.
						logged = true;
					}

				}
				else
				{
					//the params arent available
					send(client, "@PGM_ERR_REGISTERR_INFO|", strlen("@PGM_ERR_REGISTERR_INFO|"), 0);
				}
			}
			else if (messageCode == "EN_LOGIN")
			{
				if (com.size() == 3)
				{
					User* p = new User(com[1], client);
					User* k = NULL;
					bool exists = false;
					/*Username should be inside com[1]
					password should be inside com[2]
					check if this user exists on the database,implement in manager::login_user*/
					k = m->try_login(com[1], com[2]);
					if (k)
					{
						//If successfully logged - in with existing user name and password,send the "successfully logged in command:
						User* F = new User(k->getName(), p->getSocket());
						delete p;
						delete k;
						m->login_user(*F);
						res.append("@PGM_SCC_LOGIN|");
						res.append(m->list_Rooms()); //send list of rooms too
						send(client, res.c_str(), strlen(res.c_str()), 0);	//send it.
						logged = true;
					}

					else
					{
						//If it didnt log in,send the failed login command:
						send(client, "@PGM_ERR_LOGIN|", strlen("@PGM_ERR_LOGIN|"), 0);
					}
					


				}
			}
		}
		else
		{	
			if (!in_Room)
			{
				if (messageCode == "RM_ROOM_LIST")
				{
					std::string list = "@PGM_CTR_ROOM_LIST|"; //The room list command
					list.append(m->list_Rooms()); // Adding the list of rooms
					send(client, list.c_str(), strlen(list.c_str()), 0); //Sending it back to client.
				}
				else if (messageCode == "RM_CREATE_GAME")
				{
					if (com.size() != 2)
					{
						send(client, "@PGM_ERR_GAME_CREATED|", strlen("@PGM_ERR_GAME_CREATED|"), 0);
					}
					else
					{
						std::string s(com[1]);
						m->add_room(m->getUser(client), s);//Its only a room. Not a game yet. Does not require an update in the database, Unless a game is started.
						send(client, "@PGM_SCC_GAME_CREATED|", strlen("@PGM_SCC_GAME_CREATED|"), 0);
						currRoom = m->getRoom(m->getUser(client));
						in_Room = true;

					}
				}
				else if (messageCode == "RM_JOIN_GAME")
				{
					if (com.size() < 2)
					{
						//wrong params message should be sent
					}
					else
					{
						Room * r = m->getRoomByAdminName(com[1]);
						if (!r)
						{
							//send failed message to join game,room does not exist.you may have entered a invalid admin name or mystyped it.
							std::string s = "@PGM_ERR_ROOM_NOT_FOUND|";
							s.append(com[1]);
							send(client, s.c_str(), strlen(s.c_str()), 0);
						}
						else
						{
							if (r->add_user(m->getUser(client)) == true)
							{
								std::string s = "@PGM_SCC_GAME_JOIN|";
								s.append(r->getUserList());
								//sending to the client the names of the users in the room
								send(client, s.c_str(), strlen(s.c_str()), 0);


								s = "@PGM_CTR_NEW_USER|";
								s.append(m->getUser(client).getName());
								s.append("||");
								//sending to the other players in the room,that this player is in the room
								r->roomBroadCast(s);
								in_Room = true;
								currRoom = r;
								//needs to be taken care of the sql side - for niki:
							}
							else
							{
								//Notifying the client, That the room is full.
								std::string s = "@PGM_ERR_ROOM_FULL|";
								s.append(com[1]);
								send(client, s.c_str(), strlen(s.c_str()), 0);

							}
						}
					}
				}
			}
			else
			{
				if (currRoom->is_open() == true)
				{
					game(c, m);
				}
				if (messageCode == "RM_START_GAME")
				{
					if (m->RoomExists(m->getUser(client)) == true)
					{
						Room * r = m->getRoom(m->getUser(client));
						if (r->getNumOfPlayers() < 2)
						{
							send(client, "@PGM_ERR_TOO_FEW_USERS||", strlen("@PGM_ERR_TOO_FEW_USERS||"), 0);
						}
						else
						{
							m->start_game_db(*r);
						}
					}
				}
				else if (messageCode == "RM_LEAVE_GAME")
				{
					Room * r = m->inRoom(m->getUser(client));
					r->delete_user(m->getUser(client));
					std::string s = "@PGM_CTR_REMOVE_USER|";
					s.append(m->getUser(client).getName());
					s.append("||");
					r->roomBroadCast(s);
					currRoom = NULL;

					//sql side - needs to be taken care of - for niki:
				}
			}
			
		}
		
		memset(buff, 0, sizeof(buff));
		recv(client, buff, 100, 0);
		com = Explode(buff);
		messageCode = com[0];
	}
	closesocket(client);
}

void Communicate::game(connection c, Manager * m)
{










}
std::vector<std::string> Communicate::Explode(const std::string& command)
{
	const char comma = '|';
	std::string next;
	std::vector<std::string> result;
	std::string::const_iterator ourchar = command.begin();
	if (command.find("@") == std::string::npos)
	{
		std::cout << "not a valid command" << std::endl;
		result.push_back("");
	}
	else
	{
		ourchar++;
		for (ourchar = ourchar; ourchar != command.end(); ourchar++) {
			if (*ourchar == comma) {
				if (!next.empty()) {
					result.push_back(next);
					next.clear();
				}
			}
			else {
				next = next + *ourchar;
			}
		}
		if (!next.empty()) {
			result.push_back(next);
		}
		return result;
	}

	return result;
}



