
#include "connection.h"
#undef UNICODE
#define WIN32_LEAN_AND_MEAN
#define DEFAULT_BUFLEN 512
#define DEFAULT_PORT "27015"

// Need to link with Ws2_32.lib - linked with linker - need to be checked anyway later.
//#pragma comment (lib, "Mswsock.lib")

connection::connection(int port)
{
	_port = port;
	_listenSock = INVALID_SOCKET;
	_clientSock = INVALID_SOCKET;
}

struct addrinfo * connection::Init()
{
	int iResult;
	struct addrinfo *result = NULL;

	// Initialize Winsock
	iResult = WSAStartup(MAKEWORD(2, 2), &_wsa);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return NULL;
	}

	ZeroMemory(&_hints, sizeof(_hints));
	_hints.ai_family = AF_INET;
	_hints.ai_socktype = SOCK_STREAM;
	_hints.ai_protocol = IPPROTO_TCP;
	_hints.ai_flags = AI_PASSIVE;

	// Resolve the server address and port
	iResult = getaddrinfo(NULL, DEFAULT_PORT, &_hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return NULL;
	}


	return result;

}

int connection::boot(struct addrinfo * result)
{
	// Create a SOCKET for connecting to server
	int iRes;

	_listenSock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (_listenSock == INVALID_SOCKET) {
		printf("socket failed with error: %ld\n", WSAGetLastError());
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iRes = bind(_listenSock, result->ai_addr, (int)result->ai_addrlen);
	if (iRes == SOCKET_ERROR) {
		printf("bind failed with error: %d\n", WSAGetLastError());
		freeaddrinfo(result);
		closesocket(_listenSock);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);

	iRes = listen(_listenSock, SOMAXCONN);
	if (iRes == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(_listenSock);
		WSACleanup();
		return 1;
	}


	return 0;
}

int connection::shut()
{
	// shutdown the connection since we're done
	int iRes = shutdown(_clientSock, SD_SEND);
	if (iRes == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(_clientSock);
		WSACleanup();
		return 1;
	}

	// cleanup
	closesocket(_clientSock);
	WSACleanup();
	
	return 0;
}


SOCKET * connection::getClient()
{
	return &_clientSock;
}

SOCKET * connection::getServer()
{
	return &_listenSock;
}



