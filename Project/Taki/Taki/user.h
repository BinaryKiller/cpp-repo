#include <iostream>
#include <string>
#include <WinSock2.h>
#include <Windows.h>



#ifndef _my_User
#define _my_User
class User
{
public:
	User(std::string name,SOCKET sock);
	SOCKET getSocket();
	std::string getName();
private:
	std::string user_name;
	bool is_admin;
	SOCKET user_socket;
};
#endif