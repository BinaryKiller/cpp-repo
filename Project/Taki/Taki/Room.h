#include "user.h"
#include "Card.h"
#include <vector>
#include <time.h>

#ifndef _my_Room
#define _my_Room

#define Default_Way 1 
#define Stop 2 //when stop card is activated
#define Revrse_Way -1 //when direction is flipped
#define Reverse_Stop -2 //when stop card is activated while direction is flipped

class Room
{
public:

	Room(std::string name, User * ad);
	bool add_user(User& user);
	void delete_user(User& user);
	bool is_open() const;
	void close();
	bool is_in_room(const User& user) const;
	bool start_game();
	bool play_turn(const std::vector<Card> moves) const;
	bool draw_cards(int card_Number);
	int  is_turn_legal(const std::vector<Card>& moves) const;
	bool is_draw_legal(int num_of_cards) const;
	std::string getUserList();
	void roomBroadCast(std::string s);
	std::vector<Card> shuffle_cards(int num_of_cards) const;
	std::vector<std::vector<Card>> shuffle_cards_start_game(int num_of_players) const;
	std::string getAdminName();
	std::string getRoomName();
	int getNumOfPlayers();
private:

	std::string room_name;
	User* admin;
	User* players[4];
	int game_db_id;
	bool in_game;
	int current_player;
	int turn_modifier;
	int draw_counter;
	Card last_card;

};

#endif