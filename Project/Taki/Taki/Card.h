#include <iostream>
#include <string>

#ifndef _my_Card
#define _my_Card

#define RED 'r'
#define GREEN 'g'
#define YELLOW 'y'
#define BLUE 'b'
#define PLUS '+'
#define STOP '!'
#define CHANGE_DIRECTION '<'
#define PLUS2 '$'
#define CHANGE_COLOR '%'
#define TAKI '^'
#define SUPER_TAKI '*'

#define EMPTY ' '

class Card
{
public:
	Card();
	Card(char type, char color);
	std::string to_string();
private:
	char type;
	char color;

};
#endif