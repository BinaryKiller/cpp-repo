#include "Room.h"





Room::Room(std::string name, User * ad)
{
	room_name = name;
	this->admin = ad;
	for (int i = 0; i < 4; i++)
	{
		players[i] = NULL;
	}
	this->turn_modifier = Default_Way;
}
bool Room::add_user(User& user)
{
	for (int i = 0; i < 4; i++)
	{
		if (!players[i])
		{
			players[i] = &user;
			return true;//success
		}
	}

	return false;//failure,room is full probaly
}

void Room::delete_user(User& user)
{
	User* temp[4];
	bool found = false;
	int c = 0;
	for (int i = 0; (i < 4) && (!found); i++)
	{
		if (players[i] == &user)
		{
			delete players[i];
			players[i] = NULL;
			found = true;
		}
	}
	for (int i = 0; i < 4; i++)
	{
		if (players[i])
		{
			temp[c] = players[i];
			c++;
		}
	}
	for (int i = 0; i < 4; i++)
	{
		if (i < c)
		{
			players[i] = temp[i];
		}
		else
		{
			players[i] = NULL;
		}
	}

}


bool Room::is_open() const
{
	if (this->in_game)
	{
		return true;
	}

	return false;
}


void Room::close()
{
	//----handle things needed before room is closed - wip----//

	//deleting pointers and data
	delete this->admin;
	for (int i = 0; i < 4; i++)
	{
		delete players[i];
	}
	this->in_game = false;
	//handle things after room is closed.


}


bool Room::is_in_room(const User& user) const
{
	for (int i = 0; i < 4; i++)
	{
		if (players[i] == &user)
		{
			return true;
		}
	}

	return false;
}


bool Room::start_game()
{
	this->in_game = true;
	this->turn_modifier = Default_Way;
	this->current_player = 0;
	this->draw_counter = 0;
	//handle other things - wip
	return true;
}

bool Room::play_turn(const std::vector<Card> moves) const
{

	return true;
}

bool Room::draw_cards(int card_number)
{
	return true;
}

int Room::is_turn_legal(const std::vector<Card>& moves) const
{
	return 0;
}

bool Room::is_draw_legal(int num_of_cards) const
{
	return true;
}

std::string Room::getRoomName()
{
	return room_name;
}

std::string Room::getAdminName()
{
	return admin->getName();
}

int Room::getNumOfPlayers()
{
	int k = 0;
	for(int i = 0; i < 4; i++)
	{
		if (players[i])
		{
			k++;
		}
	}

	return k;
}
std::string Room::getUserList()
{
	std::string s = "";
	for (int i = 0; i < getNumOfPlayers(); i++)
	{
		s.append(players[i]->getName());
		s.append("|");
	}
	s.append("|");

	return s;
}

void Room::roomBroadCast(std::string s)
{
	for (int i = 0; i < getNumOfPlayers(); i++)
	{
		send(players[i]->getSocket(), s.c_str(), strlen(s.c_str()), 0);
	}
}


std::vector<Card> Room::shuffle_cards(int num_of_cards) const
{
	srand(time(NULL));
	char signs[11] = { RED, GREEN, YELLOW, BLUE, PLUS, STOP, CHANGE_DIRECTION, PLUS2, CHANGE_COLOR, TAKI, SUPER_TAKI };
	std::vector<Card> vec;
	for (int i = 0; i < num_of_cards; i++)
	{
		short int color = rand() % 4;
		short int type = rand() % 7 + 4;
		Card c(signs[type], signs[color]);
		vec.push_back(c);
	}

	return vec;
}

std::vector<std::vector<Card>> Room::shuffle_cards_start_game(int num_of_players) const
{
	std::vector<std::vector<Card>> vec;
	for (int i = 0; i < num_of_players; i++)
	{
		vec.push_back(shuffle_cards(8));
	}

	return vec;
}