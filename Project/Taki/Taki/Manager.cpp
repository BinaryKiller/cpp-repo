#include "Manager.h"




Manager::Manager()
{
	int rc;
	std::string sql;
	char * zErrMsg;
	rc = sqlite3_open("Taki.db", &db);
	if (rc)
	{
		std::cout << "Can't open database: " << sqlite3_errmsg(db) << std::endl;
	}
	else
	{
		std::cout << "Opened database successfully" << std::endl;

		//creating the game table
		sql = "CREATE TABLE if not exists game("  \
			"game_id INTEGER PRIMARY KEY     NOT NULL," \
			"game_start           INTEGER    NOT NULL," \
			"game_end           INTEGER    NOT NULL," \
			"turns_number            INTEGER     NOT NULL);";

		rc = sqlite3_exec(db, sql.c_str(), Manager::callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) // in case of failure
		{
			std::cout << "SQL Error: " << zErrMsg << std::endl;
			sqlite3_free(&zErrMsg);
		}
		else
		{
			std::cout << "the game table has been successfully created! " << std::endl;
		}

		//creating the users table
		sql = "CREATE TABLE if not exists users("  \
			"username           TEXT    NOT NULL," \
			"password_hash            TEXT     NOT NULL);";

		rc = sqlite3_exec(db, sql.c_str(), Manager::callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) // in case of failure
		{
			std::cout << "SQL Error: " << zErrMsg << std::endl;
			sqlite3_free(&zErrMsg);
		}
		else
		{
			std::cout << "the users table has been successfully created! " << std::endl;
		}

		//creating the user_game table
		sql = "CREATE TABLE if not exists user_game("  \
			"username        TEXT      NOT NULL," \
			"game_id      INTEGER      NOT NULL," \
			"game_end           INTEGER    NOT NULL," \
			"turns_number            INTEGER     NOT NULL," \
			"PRIMARY KEY(username, game_id));";

		rc = sqlite3_exec(db, sql.c_str(), Manager::callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) // in case of failure
		{
			std::cout << "SQL Error: " << zErrMsg << std::endl;
			sqlite3_free(&zErrMsg);
		}
		else
		{
			std::cout << "the game_user table has been successfully created! " << std::endl;
		}

	}
	gameCount = 0;
}

int Manager::callback(void *NotUsed, int argc, char **argv, char **azColName)
{
	int i;
	for (i = 0; i<argc; i++)
	{
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");

	return 0;
}


bool Manager::register_user(User &user, std::string password)
{
	int rc;
	char * zErrMsg;
	std::string sql;
	SOCKET sock = user.getSocket();

	sql = "insert into users values(";
	sql.append("'");
	sql.append(user.getName());
	sql.append("'");
	sql.append(", ");
	sql.append("'");
	sql.append(password);
	sql.append("'");
	sql.append(");");

	rc = sqlite3_exec(db, sql.c_str(), Manager::callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		std::cout << "SQL Error: " << zErrMsg << std::endl;
	}

	//user_map.emplace(user.getSocket(), password);
	//insert this data into the sql server as well,wiz.

	user_map.emplace(sock, &user);
	return 1;
}

User* Manager::try_login(std::string user_name, std::string user_password)
{
	User * u = new User(user_name, 0);
	bool exists = is_exist(*u);
	if (!exists)
	{
		return NULL;
	}
	else
	{
		int rc;
		std::string sql;
		sqlite3_stmt * stmt;
		sql = "select password_hash from users where password_hash == ";
		sql.append("'");
		sql.append(user_password);
		sql.append("'");
		sql.append(";");

		rc = sqlite3_prepare(db, sql.c_str(), sql.length(), &stmt, NULL);
		sqlite3_step(stmt);
		sql = (char *)sqlite3_column_text(stmt, 0);
		if (sql.find(user_password) != std::string::npos)
		{
			return u;
		}
	}


	return NULL;
}

void Manager::login_user(User &user)
{
	SOCKET sock = user.getSocket();
	user_map.emplace(sock,&user);
}


User& Manager::getUser(SOCKET sock)
{
	return *user_map[sock];
}

bool Manager::is_exist(User &user) const
{
	int rc;
	bool exists = false;
	const char * z;
	//check on database if exists - work for wiz
	std::string sql;
	sqlite3_stmt * stmt;
	sql = "select username from users where username == ";
	sql.append("'");
	sql.append(user.getName());
	sql.append("'");
	sql.append(";");
	
	rc = sqlite3_prepare(db, sql.c_str(), sql.length(), &stmt, &z);
	if (rc == SQLITE_OK)
	{
		sqlite3_step(stmt);
		sql = (char *)sqlite3_column_text(stmt, 0);
		sqlite3_finalize(stmt);
		if (sql.find(user.getName()) != std::string::npos)
		{
			exists = true;
		}
	}
	
	


	return exists;
}
std::string Manager::list_Rooms()
{
	std::string res = "";
	std::string d = "|";
	for (std::vector<Room>::iterator it = room_vector.begin(); it != room_vector.end(); it++)
	{
		res.append(it->getRoomName());
		res.append(d);
		res.append(it->getAdminName());
		res.append(d);
		res.append(std::to_string(it->getNumOfPlayers()));
		res.append(d);
	}

	return res;
}


bool Manager::add_room(User &admin, std::string room_name)
{

	Room r(room_name, &admin);
	room_vector.push_back(r);
	//this is just a room.it does not need to be on the games table.it will become a game only if the client starts a game.and the update in the database occurs on the function start_game_db in this class(Room).

	return true;
}

Room * Manager::getRoom(User& Admin)
{
	for (unsigned int i = 0; i < room_vector.size(); i++)
	{
		if (room_vector.at(i).getAdminName() == Admin.getName())
		{
			return &room_vector.at(i);
		}
	}
	
	return NULL;
}

bool Manager::RoomExists(User& Admin)
{
	for (std::vector<Room>::iterator it = room_vector.begin(); it != room_vector.end(); it++)
	{
		if (Admin.getName() == it->getAdminName())
		{
			return true;
		}
	}
	return false;
}


Room* Manager::getRoomByAdminName(std::string name)
{
	for (unsigned int i = 0; i < room_vector.size(); i++)
	{
		if (room_vector.at(i).getAdminName() == name)
		{
			return &room_vector.at(i);
		}
	}

	return NULL;
}



Room * Manager::inRoom(User& user)
{
	for (std::vector<Room>::iterator it = room_vector.begin(); it != room_vector.end(); it++)
	{
		if (it->is_in_room(user))
		{
			return &(*it);
		}
	}

	return NULL;
}

bool Manager::start_game_db(const Room &room) const
{
	std::string com;
	char * zErrMsg;
	int ok;
	if (gameCount == 0)
	{
		com = "insert into games(game_id,game_start) values(0,strftime('%s','now'));";
		ok = sqlite3_exec(this->db, com.c_str(), callback, 0, &zErrMsg);
		if (ok != SQLITE_OK)
		{
			std::cout << "SQL Error: " << zErrMsg << std::endl;
			sqlite3_free(&zErrMsg);
			return false;
		}
	}
	else
	{
		com = "insert into games(game_start) values(strftime('%s','now'));";
		ok = sqlite3_exec(this->db, com.c_str(), callback, 0, &zErrMsg);
		if (ok != SQLITE_OK)
		{
			std::cout << "SQL Error: " << zErrMsg << std::endl;
			sqlite3_free(&zErrMsg);
			return false;
		}
	}
	

	return true;
}

/*
//thats for later --------------------------------

bool Manager::remove_room(Room &room)
{

}




bool Manager::end_game_db(const Room &room) const
{

}

void Manager::client_requests_thread(SOCKET sock)
{

}*/

