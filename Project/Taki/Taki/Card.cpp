#include "Card.h"

Card::Card(char type = EMPTY, char color = EMPTY)
{
	this->type = type;
	this->color = color;
}

Card::Card()
{
	this->type = EMPTY;
	this->color = EMPTY;
}
std::string Card::to_string()
{
	std::string f = "";
	f.push_back(this->type);
	f.push_back(this->color);
	return f;
}