

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <vector>
#include <thread>

#define portie "28000"
#define LEN 512

void task(SOCKET * C_Sock);

int main(void)
{
	WSADATA wsaData;
	int iResult;

	SOCKET lSock = INVALID_SOCKET;
	SOCKET cSock = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[LEN];
	int recvbuflen = LEN;

	
	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult) {
		std::cout << "the function: WSAStartup have failed with: " << iResult << std::endl;
		return 1;
	}
	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;
	// Resolve the server address and port
	iResult = getaddrinfo(NULL, portie, &hints, &result);
	if (iResult != 0) {
		std::cout << "getaddrinfo failed with error: "  << iResult << std::endl;
		WSACleanup();
		return 1;
	}

	// Create a SOCKET for connecting to server
	lSock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (lSock == INVALID_SOCKET) {
		std::cout << "the function socket have failed with error: " << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		WSACleanup();
		return 1;
	}

	// Setup the TCP listening socket
	iResult = bind(lSock, result->ai_addr, (int)result->ai_addrlen);
	if (iResult == SOCKET_ERROR) {
		std::cout << "the bind function has failed with error number" << WSAGetLastError() << std::endl;
		freeaddrinfo(result);
		closesocket(lSock);
		WSACleanup();
		return 1;
	}

	freeaddrinfo(result);
	
	iResult = listen(lSock, SOMAXCONN);
	if (iResult == SOCKET_ERROR) 
	{
		std::cout << "listen has failed with error: " << WSAGetLastError() << std::endl;
		closesocket(lSock);
		WSACleanup();
		return 1;
	}

	int i = 0;
	while (i < 4)
	{
		cSock = accept(lSock, NULL, NULL);
		if (cSock != INVALID_SOCKET)
		{
			std::thread t1(task, &cSock);
			t1.detach();
			i++;
		}
		
	}
	return 0;
}


void task(SOCKET * C_Sock)
{
	int res;

	res = send(*C_Sock, "Accepted.", 9, 0);
	printf("accepted sent\n");
	if (res == SOCKET_ERROR) {
		printf("send failed with error: %d\n", WSAGetLastError());
		closesocket(*C_Sock);
		WSACleanup();
		return;
	}
	


	// shutting down the connection
	res = shutdown(*C_Sock, SD_SEND);
	if (res == SOCKET_ERROR) {
		printf("shutdown failed with error: %d\n", WSAGetLastError());
		closesocket(*C_Sock);
		WSACleanup();
		return;
	}

	closesocket(*C_Sock);

}