#include <thread>
#include <mutex>
#include <iostream>
#include <vector>
#include <condition_variable>
#include <string>

using namespace std;

void read();
void write();

int readers = 0;
int writers = 0;
int Bytes = 3;

bool writing = false;
bool reading = false;
bool isEmpty = false;
mutex r_Mtx;
condition_variable r_Cond;
mutex w_Mtx;
condition_variable w_Cond;

int main()
{
	/*this example should show us how this lock works.if the file is empty,it will not let readers to read anymore,until someone writes to it.in this test,the file has 3 bytes.
	each reading takes 1 byte only.after 3 readings,t4(the 4th reader) wont be able to read until someone writes on the file.he will just sleep.
	in this example t5 will write,and only after t5 will go to sleep,t4 will wake up and continue reading,because now there are bytes to read.*/

	/* A reader will go to sleep when: Someone else reads, There is nothing to read, Or if someone writes. He will wake up when: No one writes and no one reads, And there is what to read.*/
	/*a writer will go to "sleep" , As long as there are active readers. He might wake up when: The file is empty , Or when there are no readers.*/

	thread t1(read);
	thread t2(read);
	thread t3(read);
	thread t4(read);
	thread t5(write);

	t1.join();
	t2.join();
	t3.join();
	t4.join();
	t5.join();
	system("pause");
	return 0;
}



void read()
{
	unique_lock<mutex> ul(r_Mtx,defer_lock);
	readers++;
	ul.lock();
	if ((isEmpty) || writing || (readers > 1))
	{
		r_Cond.wait(ul, []{return ((!isEmpty) && (!writing)); });
		writing = false;
		reading = true;
	}
	reading = true;
	cout << "reading from file!" << endl;
	this_thread::sleep_for(chrono::milliseconds(20));
	Bytes--;
	if (!Bytes)
	{
		isEmpty = true;
	}
	cout << "reading done!" << endl;
	readers--;
	ul.unlock();
	
	r_Cond.notify_one();
	w_Cond.notify_one();
}


void write()
{
	unique_lock<mutex> ul(w_Mtx, defer_lock);
	ul.lock();
	writers++;
	if (readers > 0)
	{
		w_Cond.wait(ul, []{return (readers == 0 || writing || isEmpty); });
		reading = false;
		writing = true;
	}
	writing = true;
	cout << "writing to file!" << endl;
	this_thread::sleep_for(chrono::milliseconds(20));
	Bytes += 2;
	isEmpty = false;
	cout << "writing done!" << endl;
	writers--;
	ul.unlock();
	if (!writers)
	{
		writing = false;
	}
	w_Cond.notify_one();
	r_Cond.notify_one();
	
	
}


